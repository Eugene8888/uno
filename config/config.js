require('dotenv').config();
module.exports = {
    "development": {
        "use_env_variable": "DATABASE_URL",
        "dialect": "postgres",
        "ssl":true,
        "dialectOptions":{
            "ssl":{
                "require":true
            }
        }
    },
    "test": {
        "use_env_variable": "DATABASE_URL",
        "dialect": "postgres"
    },
    "production": {
        "use_env_variable": "DATABASE_URL",
        "dialect": "postgres"
    }
}