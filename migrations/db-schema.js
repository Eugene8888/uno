
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.createTable('user',
            {
                user_id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                username: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                password: {
                    type: Sequelize.STRING,
                    allowNull: false
                },
                email: {
                    type: Sequelize.STRING,
                    allowNull: false
                }
            }),
            queryInterface.createTable('game',
            {
                game_id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                state: {
                    type: Sequelize.BOOLEAN, //false: waiting   true: active
                    defaultValue: false,
                    allowNull: false
                },
                color: {
                    type: Sequelize.STRING,
                    allowNull: ture
                },
                direction: {
                    type: Sequelize.BOOLEAN, //false: clockwise   true: conterclockwise
                    defaultValue: false,
                    allowNull: false
                },
                capacity: {
                    type: Sequelize.INTEGER, //should make it final value?
                    defaultValue: 4,
                    allowNull: false
                },
                current_player_count: {
                    type: Sequelize.INTEGER,
                    defaultValue: 0,
                    allowNull: false
                }
            }),
            queryInterface.createTable('game_user',
            {
                game_user_id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                user_id: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'user',
                        key: 'user_id'
                    },
                    allowNull: false
                },
                game_id: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'game',
                        key: 'game_id'
                    },
                    allowNull: false
                },
                penalty_not_yelling_uno: {
                    type: Sequelize.BOOLEAN,
                    defaultValue: false,
                    allowNull: false
                },
                start_game: {
                    type: Sequelize.BOOLEAN, //false: not started     true: started
                    defaultValue: false,
                    allowNull: false
                },
                active: {
                    type: Sequelize.BOOLEAN, //false: not the user's turn     true: the user's turn
                    defaultValue: false,
                    allowNull: false
                }
            }),
            queryInterface.createTable('card',
            {
                card_id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                color: {
                    type: Sequelize.STRING, //R B Y G
                    allowNull: true
                },
                number: {
                    type: Sequelize.INTEGER, //0~9
                    allowNull: true
                },
                text: {
                    type: Sequelize.STRING, //reverse, wildcard, wildcard drawfour, drawtwo
                    allowNull: false
                }
            }),
            queryInterface.createTable('game_user_card',
            {
                card_id: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'card',
                        key: 'card_id'
                    },
                    allowNull: false
                },
                user_id: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'user',
                        key: 'user_id'
                    },
                    allowNull: false
                },
                game_id: {
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'game',
                        key: 'game_id'
                    },
                    allowNull: false
                },
                discard: {
                    type: Sequelize.BOOLEAN, //false: waiting   true: active
                    defaultValue: false,
                    allowNull: false
                },

            })
        ]).then(function () {
            const Tasks = queryInterface('card').insert([
                {color: "R", number: 0},
                {color: "R", number: 1}
            ])

        })
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.dropTable('user'),
            queryInterface.dropTable('game'),
            queryInterface.dropTable('game_user'),
            queryInterface.dropTable('card')

        ])
    }
};
