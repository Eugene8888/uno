
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.createTable('user',
                {
                    user_id: {
                        type: Sequelize.INTEGER,
                        primaryKey: true,
                        autoIncrement: true
                    },
                    username: {
                        type: Sequelize.STRING,
                        allowNull: false
                    },
                    password: {
                        type: Sequelize.STRING,
                        allowNull: false
                    },
                    email: {
                        type: Sequelize.STRING,
                        allowNull: false
                    }
                }),
            queryInterface.createTable('game',
                {
                    game_id: {
                        type: Sequelize.INTEGER,
                        primaryKey: true,
                        autoIncrement: true
                    },
                    state: {
                        type: Sequelize.BOOLEAN, //false: waiting   true: active
                        defaultValue: false,
                        allowNull: false
                    },
                    color: {
                        type: Sequelize.STRING,
                    },
                    number: {
                        type: Sequelize.INTEGER,
                    },
                    top_card: {
                        type: Sequelize.INTEGER,
                    },
                    direction: {
                        type: Sequelize.BOOLEAN, //false: clockwise   true: conterclockwise
                        defaultValue: false,
                        allowNull: false
                    },
                    capacity: {
                        type: Sequelize.INTEGER, //should make it final value?
                        defaultValue: 4,
                        allowNull: false
                    },
                    current_player_count: {
                        type: Sequelize.INTEGER,
                        defaultValue: 0,
                        allowNull: false
                    }
                }),
            queryInterface.createTable('game_user',
                {
                    game_user_id: {
                        type: Sequelize.INTEGER,
                        primaryKey: true,
                        autoIncrement: true
                    },
                    user_id: {
                        type: Sequelize.INTEGER,
                        references: {
                            model: 'user',
                            key: 'user_id'
                        },
                        allowNull: false
                    },
                    username: {
                        type: Sequelize.STRING,
                        allowNull: false
                    },
                    game_id: {
                        type: Sequelize.INTEGER,
                        references: {
                            model: 'game',
                            key: 'game_id'
                        },
                        allowNull: false
                    },
                    played: {
                        type: Sequelize.BOOLEAN,
                        defaultValue: false,
                        allowNull: false
                    },
                    start_game: {
                        type: Sequelize.BOOLEAN, //false: not started     true: started
                        defaultValue: false,
                        allowNull: false
                    },
                    active: {
                        type: Sequelize.BOOLEAN, //false: not the user's turn     true: the user's turn
                        defaultValue: false,
                        allowNull: false
                    },
                    seat: {
                        type: Sequelize.INTEGER,
                        allowNull: false
                    },
                    skip: {
                        type: Sequelize.BOOLEAN, //false: not started     true: started
                        defaultValue: false,
                        allowNull: false
                    },
                    actioned: {
                        type: Sequelize.BOOLEAN, //false: not started     true: started
                        defaultValue: false,
                        allowNull: false
                    }
                }),
            queryInterface.createTable('card',
                {
                    card_id: {
                        type: Sequelize.INTEGER,
                        primaryKey: true,
                        autoIncrement: true
                    },
                    color: {
                        type: Sequelize.STRING, //R B Y G
                        allowNull: true
                    },
                    number: {
                        type: Sequelize.INTEGER, //0~9
                        allowNull: true
                    },
                    text: {
                        type: Sequelize.STRING, //reverse, wildcard, wildcard drawfour, drawtwo
                        allowNull: true
                    }
                }),
            queryInterface.createTable('game_user_card',
                {
                    card_id: {
                        type: Sequelize.INTEGER,
                        references: {
                            model: 'card',
                            key: 'card_id'
                        },
                        allowNull: false
                    },
                    user_id: {
                        type: Sequelize.INTEGER,
                        references: {
                            model: 'user',
                            key: 'user_id'
                        },
                        allowNull: true
                    },
                    game_id: {
                        type: Sequelize.INTEGER,
                        references: {
                            model: 'game',
                            key: 'game_id'
                        },
                        allowNull: false
                    },
                    discard: {
                        type: Sequelize.BOOLEAN,
                        defaultValue: false,
                        allowNull: false
                    },

                })
        ])
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.dropTable('user'),
            queryInterface.dropTable('game'),
            queryInterface.dropTable('game_user'),
            queryInterface.dropTable('card')

        ])
    }
};
