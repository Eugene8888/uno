# Uno Game ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

Uno Game is the term project for CSC 667 

## Installation

### Requirements
* node
* npm

1. clone this repo
2. cd to the root folder
3. run '$ echo DATABASE_URL=postgres://pzvfecxzmliurh:80f60d22cf240b321d1711d870f31c9ba416e25e79e29b3d04a02597259810ef@ec2-54-163-246-5.compute-1.amazonaws.com:5432/d8r2fdm0r5c69i >> .env'
4. run `$ npm install ` in root folder 

## Run

use `npm run dev` to start the dev server

## Front-End
* every HTML file should have its own css file and js file with the same name but different extendsion
`for Example, index.html should index.css and index.js  `
* all css files should be under public/css
* all js files should be under public/js

## Back-End
* server entery `index.js`
* when installing new dependencies, must be saved it to `package.json` by using `npm install {package name } --save`
* development dependencies must be saved in `devDependencies` by using `npm install {package name} --save-dev`
* all `API` files should be under `/api`
* All other helper function file should be saved under `/helper`


