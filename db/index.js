const pgp = require('pg-promise')();
require('dotenv').config();
console.log(process.env.DATABASE_URL)
const connection = pgp(process.env.DATABASE_URL + '?ssl=true');
module.exports = connection;