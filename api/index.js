const express = require( 'express');
const router = express.Router();
const user = require('./routes/user');
const game = require('./routes/game');
const gameRoom = require('./routes/gameRoom');
const gameLogic = require('./routes/gametest');
const card = require('./routes/card');
const chat = require('./routes/chat')
user(router);
game(router);
 gameRoom(router);
 gameLogic(router);
card(router);
chat(router)

module.exports = router;