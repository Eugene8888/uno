const db = require('../../db/index');
const dbUtil = require('../../game_logic/gameLogicDBUtils');
const gameLogic = require('../../game_logic/gameLogic');
const async = require('async')
const route = function(router) {
    router.put('/gameroom/:game_id/ready',(req,res)=>{
            const {user_id} = req.user;
            const {game_id} = req.params;
            // const user_id = 17;
            // const game_id = 3;
            dbUtil.setPlayerStart(user_id, game_id).then((response)=>{
                 dbUtil.getStartedGamePlayers(game_id).then((players)=>{
                     if(players.length === 4){
                         dbUtil.setGameStart(game_id).then((response) => {
                             gameLogic.initializeDeck(game_id).then(async () => {
                                 const topCardId = (game_id + 3) * 30 % 76 + 1;
                                 await gameLogic.drawFirstCard(topCardId, game_id);
                                 async.eachSeries(players, (player, callback) => {
                                         gameLogic.drawCard(player.user_id, game_id, 7, callback)
                                     },
// optional callback
                                     function (err, results) {
                                         if (err) {
                                             res.status(500).end()
                                         } else {

                                                 req.io.sockets.in(req.params.game_id).emit('game:start','game started')
                                             res.end();
                                             // dbUtil.getAllPlayerCard(game_id, user_id).then((myOwncards) => {
                                             //     console.log('cards',myOwncards)
                                             //     console.log('emitting start game');
                                             //     req.io.sockets.in(req.params.game_id).emit('game:start', {
                                             //         topCard: topCardId,
                                             //         mycards: myOwncards
                                             //     })
                                             //     res.send('{card_id : ' + topCardId + '}')
                                             // });

                                         }
                                     });
                                 dbUtil.getPlayerBySeat(game_id, 1).then((user) => {
                                     dbUtil.setActivePlayer(user.user_id, game_id);
                                 })
                             })
                             req.io.sockets.in(req.params.game_id).emit('player:start', {
                                 msg: req.body.message,
                                 username: req.user.username
                             });
                             // res.end();
                         })
                     }
                 }).catch((err)=>{
                     console.log(err)
                 })
            }).catch(error => {
                    console.log('ERROR:', error); // print error;
                    res.status(500).end()
                });
    })

    router.post('/gameroom/:game_id/playcard', async (req, res) => {
        const {user_id} = req.user;
        const {game_id} = req.params;
        const {card_id} = req.body;
        // const user_id = 17;
        // const game_id = 3;
        // const card_id = 85;
        const {color} = req.body;

        const currentActivePlayer = await dbUtil.getActivePlayer(game_id);
        // console.log("1", currentActivePlayer);
        const isValid = await gameLogic.isValid(game_id, user_id, card_id);
        console.log("2", isValid);
        const nextPlayer = await gameLogic.getNextPlayer(user_id, game_id);
        console.log("3", nextPlayer);
        console.log(currentActivePlayer, "," + isValid + "," + nextPlayer);
        if (currentActivePlayer.user_id === user_id && isValid) {
            dbUtil.getCard(card_id).then(async (playedCard) => {
                console.log("played card: ",  playedCard)
                if (playedCard.text === "null") { //0~9
                    await gameLogic.playNumberCard(user_id, game_id, card_id);
                    console.log("number care played")
                } else if (playedCard.text === "reverse") {
                    await gameLogic.playReverseCard(user_id, game_id, card_id)
                } else if (playedCard.text === "skip") {
                    await gameLogic.playSkipCard(user_id, game_id, card_id)
                } else if (playedCard.text === "draw2") {
                    await gameLogic.playDraw2Card(user_id, game_id, card_id, nextPlayer)
                } else if (playedCard.text === "wildcard") {
                    //active player set the color
                    await gameLogic.playWildcard(user_id, game_id, card_id, color)
                } else if (playedCard.text === "wildcard4") { //wildcard4
                    //active player set the color and next player is skipped
                    await gameLogic.playWildcard4(user_id, game_id, card_id, color, nextPlayer)
                } else {
                     res.send("Invalid card")
                }
            }).then(async () => {
                const game = await dbUtil.getGame(game_id);
                console.log("game", game)
                await dbUtil.setGameTopCard(card_id, game_id);
                console.log("top card set")
                dbUtil.discardCard(user_id, game_id, card_id).then(async () => {
                    const cardcount = await dbUtil.getAllPlayerCard(game_id, user_id);
                    const win = await dbUtil.getAllPlayerCard(game_id, user_id);
                    console.log(cardcount + "," + win);
                    if (win.length === 0) {
                        gameLogic.finishGame(user_id, game_id).then(() => {
                            req.io.sockets.in(game_id).emit("game:status", {status: "Game Over"})
                            // res.send("GameOver")
                        })
                    } else {
                        // dbUtil.setPlayedStatus(user_id, game_id).then(async () => {
                        dbUtil.setPlayedStatus(user_id, game_id);
                        // dbUtil.getGame(game_id).then((game)=>{
                        console.log("game", game);
                        // console.log(game.direction)
                        // console.log("nextplayer: " + nextPlayer);
                        // console.log("cardcount.length: " + cardcount.length)

                        let  allGamePlayers = await dbUtil.getGamePlayer(game_id);
                        async.each(allGamePlayers, function(user, callback) {
                            console.log("allGamePlayers", allGamePlayers)
                            dbUtil.getAllPlayerCard(game_id, user.user_id).then((cards)=>{
                                console.log("all user cards: ", cards);
                                user.cards = cards
                                callback()
                            })
                        }, function(err) {

                            req.io.sockets.in(game_id).emit('game:card', {
                                type: 'update',
                                game,
                                // user: {user_id, cards: cardcount},
                                allGamePlayers,
                                topcard: card_id
                            }) // need to be fixed
                            // res.send({started:true,game,allGamePlayers})
                        });
                        // req.io.sockets.in(game_id).emit('game:card', {
                        //     type: 'update',
                        //     game,
                        //     user: {user_id, cards: cardcount},
                        //     topcard: card_id
                        // }) // need to be fixed
                        res.send({valid: true})
                        // })
                    }
                })
            })
        } else {
            res.send({valid : false})
            // if (!isValid) {
            //     res.send({valid : false})
            // } else {
            //     res.send("Not active")
            // }
        }
    })

    router.post('/gameroom/:game_id/drawcard', async (req, res) => {//active player can draw any number of cards at any time
        const {user_id} = req.user;
        const {game_id} = req.params;
        // const user_id = 17;
        // const game_id = 3;
        const currentActivePlayer = await dbUtil.getActivePlayer(game_id);
        if (currentActivePlayer.user_id === user_id) {
            // gameLogic.drawCard(user_id, game_id, 1).then(() => {
            //     dbUtil.setActioned(user_id, game_id);
            // }).then(response => {
            await gameLogic.drawCard(user_id, game_id, 1, function(){

                dbUtil.setActioned(user_id, game_id).then(() => {
                    dbUtil.getAllPlayerCard(game_id, user_id).then((allCards)=>{
                        // console.log("allCards.length: " + allCards.length)
                        req.io.sockets.in(game_id).emit("game:card",{ type:'drawcard', data :{user_id, updatedCardSet:allCards }}) // need to be fixed
                    })
                    res.send("OK")
                }).catch(error => {
                    res.status(500).end()
                })


            });

        } else {
            res.send("not active");
        }
    })

    router.post('/gameroom/:game_id/endturn', async (req, res) => {
        const {user_id} = req.user;
        const {game_id} = req.params;
        // const user_id = 17;
        // const game_id = 25;
        const currentActivePlayer = await dbUtil.getActivePlayer(game_id);
        const {actioned} = await dbUtil.getActionedStatus(game_id, user_id);
        // console.log("currentActive: " + currentActivePlayer.user_id);
        if (currentActivePlayer.user_id === user_id && actioned) {
         dbUtil.unsetActioned(user_id, game_id).then(() => {
               gameLogic.passToNextPlayer(user_id, game_id,(activePlayer_id)=>{
                   console.log("activePlayer_id: " + activePlayer_id)
                   req.io.sockets.in(game_id).emit("game:turn",{ activePlayer_id }) // need to be fixed
                        res.send("passed");
               })
                //     .then((activePlayer_id) => {
                //
                //     req.io.sockets.in(game_id).emit("game:turn",{ activePlayer_id }) // need to be fixed
                //     res.send("passed");
                // })
            })
            // console.log("already actioned");
        } else {
            res.send("not active or not actioned");
        }
    })



    //getter for frontend page
    router.get('/gameroom/:game_id/getcolor',(req,res)=>{  //{"color":"R"}
        const {game_id} = req.params;
        dbUtil.getGameColor(game_id).then(data =>{
            res.send(data);
        }).catch(error => {
            console.log('ERROR:', error); // print error;
            res.status(500).end()
        });
    })
    router.get('/gameroom/:game_id/getnumber',(req,res)=>{  //{"number":"3"}
        const {game_id} = req.params;
        dbUtil.getGameNumber(game_id).then(data =>{
            res.send(data);
        }).catch(error => {
            console.log('ERROR:', error); // print error;
            res.status(500).end()
        });
    })
    router.get('/gameroom/:game_id/getdirectionr',(req,res)=>{  //{"direction":false}
        const {game_id} = req.params;
        dbUtil.getGameDir(game_id).then(data =>{
            res.send(data);
        }).catch(error => {
            console.log('ERROR:', error);
            res.status(500).end()
        });
    })
    router.get('/gameroom/:game_id/playerCards',(req,res)=>{  //[{"card_id":2},{"card_id":3}]
        const {user_id} = req.user;
        const {game_id} = req.params;
        // const user_id = 17;
        // const game_id = 8;
        dbUtil.getAllPlayerCard(game_id, user_id).then(data =>{
            res.send(data);
        }).catch(error => {
            console.log('ERROR:', error);
            res.status(500).end()
        });
    })
    router.get('/gameroom/:game_id/activePlayer',(req,res)=>{  //{"user_id":11}
        const {game_id} = req.params;
        dbUtil.getActivePlayer(game_id).then(data =>{
            res.send(data);
        }).catch(error => {
            console.log('ERROR:', error);
            res.status(500).end()
        });
    })

    router.get('/gameroom/:game_id/getseat',(req,res)=>{  //{ seat: 1 }
        const {user_id} = req.user;
        const {game_id} = req.params;
        dbUtil.getSeat(game_id, user_id).then(data =>{
            res.send(data);
        }).catch(error => {
            console.log('ERROR:', error);
            res.status(500).end()
        });
    })
    router.get('/gameroom/:game_id/getallplayers',(req,res)=>{  //{ seat: 1 }
        const {game_id} = req.params;
        dbUtil.getGamePlayer(game_id).then(data =>{
            res.send(data);
        }).catch(error => {
            console.log('ERROR:', error);
            res.status(500).end()
        });
    })
    router.get('/gameroom/:game_id/getgamestate',(req,res)=>{
        const {game_id} = req.params;
        dbUtil.getGameState(game_id).then(data => {
            res.send(data);
        }).catch(error => {
            console.log('ERROR:', error);
            res.status(500).end()
        });
    })
    router.get('/gameroom/:game_id/getcardsbyseatnumber', (req, res) => { //{return the number of cards}
        const {game_id} = req.params;
        const {seat_id} = req.params;
        dbUtil.getPlayerBySeat(game_id, seat_id).then(player => {
            dbUtil.getAllPlayerCard(game_id, player).then((allCards) => {
                res.send({count : allCards.length});
            }).catch(error => {
                console.log('ERROR:', error);
                res.status(500).end()
            })
        })
    })
    router.get('/gameroom/:game_id/getAllGameInfo', async (req, res) => {
        const {game_id} = req.params;
        const {user_id} = req.user;
        const {seat_id} = req.params;
        // let user_id =11
        const game = await dbUtil.getGame(game_id);
        console.log("1.game:",game);
        let  allGamePlayers = await dbUtil.getGamePlayer(game_id);
        console.log("2.allgameUsers:", allGamePlayers);
        console.log("3.game.state:", game.state)
        if (game.state) {
            console.log("4: ")
            const currentActive = await dbUtil.getActivePlayer(game_id);
            console.log("currentActive", currentActive);

            async.each(allGamePlayers, function(user, callback) {
                console.log("allGamePlayers", allGamePlayers)
                dbUtil.getAllPlayerCard(game_id, user.user_id).then((cards)=>{
                    console.log("all user cards: ", cards);
                            user.cards = cards
                        callback()
                        })
            }, function(err) {
                res.send({started:true,game,allGamePlayers,currentActive})
            });
            // for(let user of allGamePlayers){
            //     dbUtil.getAllPlayerCard(game_id, user.user_id).then((cards)=>{
            //         console.log(cards)
            //         user.cards = cards
            //     })
            // }




            
        } else {
            res.send({allGamePlayers,started:false})
        }

    })
};
module.exports = route;

