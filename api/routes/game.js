const db = require('../../db/index');
const dbUtil = require('../../game_logic/gameLogicDBUtils');
const async = require('async')
//TODO: db operation didn't check for duplicates


const route = function(router) {
    router.get('/gameroom',(req,res)=>{
        db.many(`SELECT game_id FROM "public"."game"`).then((data)=>{
            res.send(data)
        }).catch((e)=>{
            console.log(e)
        })
    })
    router.post('/newgame',(req,res)=>{
        //sql create game row
        db.one(`INSERT INTO "public"."game" ("game_id", "state", "color", "direction", "capacity", "current_player_count") VALUES (DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT) RETURNING *`)
            .then(data => {
                console.log(data)
                res.send(data)
            })
            .catch(error => {
                console.log('ERROR:', error); // print error;
                res.status(500).end()
            });
    });

    router.post('/joingame',(req,res)=>{
        const { user_id: userId } = req.user;
        const { game_id: gameId } = req.body;
        const {username} = req.user;
        dbUtil.getGameState(gameId).then(status => {
            console.log(status);
            if (!status.state) { //not start
                dbUtil.getIsInGame(gameId, userId).then(exist => {
                    // console.log(exist)
                    if (exist.count === '0') { //not in the game yet
                        dbUtil.getGamePlayer(gameId).then(allplayers => {
                            console.log(allplayers)
                            console.log("num of current player: " + allplayers.length)
                            if (allplayers.length === 4) {
                                res.send("Game full");
                            } else {
                                console.log("new user seat: " + (allplayers.length + 1));
                                dbUtil.addUserToGame(userId, username,gameId,  allplayers.length + 1).then(() => {

                                    async.series([
                                        (cb)=>{
                                            allplayers.push({username,userId, seat: allplayers.length + 1});
                                            req.io.sockets.in(gameId).emit('game:allplayers', {
                                                players: allplayers
                                            });
                                            cb()
                                        }
                                    ],function(err,result){
                                        res.end()
                                    })










                                }).catch(error => {
                                    console.log(error)
                                    res.status(500).end()});
                            }
                        }).catch(error => {res.status(500).end()});
                    } else {
                        // console.log('/game/' + gameId);
                        res.redirect('/game/' + gameId);
                    }
                }).catch(error => {res.status(500).end()});
            } else { //already start
                dbUtil.getIsInGame(gameId, userId).then(exist => {
                    console.log(exist)
                    if (exist.count === '0') {//not in the game yet
                        console.log(exist);
                        res.send("Game already started; please join other game")
                    } else {
                        console.log('redirect: /game/' + gameId);
                        res.redirect('/game/' + gameId);
                    }
                });
            }
        })
        //
        // console.log(playerCount + ", " + getIsInGame + ", " + gameState)
        // if (gameState === false) {
        //     if (playerCount === 4) {
        //         res.send("Game full")
        //     } else {
        //         dbUtil.addUserToGame(userId, gameId).then(response => {
        //             res.send("Game updated successfully")
        //         }).catch(error => {
        //             console.log('ERROR:', error); // print error;
        //             res.status(500).end()
        //         });
        //     }
        // } else {
        //     if (getIsInGame === 0) {
        //         res.send("Game already started; please join other game")
        //     } else { //user retrieve game status
        //         res.redirect('/gameroom/(${gameId})', gameId);
        //     }
        // }
        // db.any(`SELECT user_id FROM game_user WHERE user_id = ${userId} AND game_id = ${gameId}`).then((response)=>{
        //
        //     if(response){
        //
        //     }else {
        //         //create new game_user
        //         db.one(`INSERT INTO "public"."game_user" ("game_user_id", "user_id", "game_id", "penalty_not_yelling_uno", "start_game", "active") VALUES (DEFAULT, '${userId}', '${gameId}', DEFAULT, DEFAULT, DEFAULT) RETURNING game_user_id`).then(()=>{
        //             db.one(`UPDATE "public"."game" SET "current_player_count" = "current_player_count" + 1 WHERE "game_id" = '${gameId}' RETURNING current_player_count`)
        //                 .then(data => {
        //                     console.log("create new")
        //                     console.log(data); // print current_player_count;
        //                     res.send('Game updated successfully')
        //
        //                 })
        //                 .catch(error => {
        //                     console.log('ERROR:', error); // print error;
        //                     res.status(500).end()
        //                 });
        //         }).catch((err)=>{
        //             console.log(err)
        //         })
        //
        //
        //     }
        // }).catch((error)=>{
        //     console.log('ERROR:', error); // print error;
        //     res.status(500).end()
        // })
    });


    router.delete('/deletegame', (req, res) => {
        const {game_id: gameId} = req.body;
        dbUtil.deleteGame(gameId).then(response => {
            res.send("Game ", gameId, " is deleted.");
        }).catch(error => {
            console.log('ERROR:', error); // print error;
            res.status(500).end()
        });
    });


    router.get('/getallgames', (req, res) => {
        // db.many(`SELECT t.* FROM public.game_user t`);
    });


    router.delete('/deletegameuser', (req, res) => {
        const {game_id: gameId} = req.body;
        const {user_id: userId} = req.user;
        dbUtil.deleteGameUser(gameId, userId).then(response => {
            res.send("Game user " + gameUserID + " is deleted.");
        }).catch(error => {
            console.log('ERROR:', error); // print error;
            res.status(500).end()
        });
    });
    router.post('/socket',(req,res)=>{
        console.log('socket')
        req.io.sockets.in(4).emit('game:start', {topCard:71})
        res.end()
    })
}

module.exports = route;