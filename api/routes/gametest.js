//This router is for testing purpose only!!















const dbUtil = require('../../game_logic/gameLogicDBUtils');
const gameLogic = require('../../game_logic/gameLogic');
const async = require('async')

const route = function(router) {

    router.post('/playcard', async (req, res) => {
        const game_id = 8;
        const user_id = 15;
        const card_id = 3;
        const color = 'R';
        const currentActivePlayer = await dbUtil.getActivePlayer(game_id);
        const isValid = await gameLogic.isValid(game_id, card_id);
        if (currentActivePlayer.user_id === user_id && isValid) {
            dbUtil.getCard(card_id).then(playedCard => {
                if (playedCard.text === "null") { //0~9
                    gameLogic.playNumberCard(user_id, game_id, card_id);
                } else if (playedCard.text === "reverse") {
                    gameLogic.playReverseCard(user_id, game_id, card_id)
                } else if (playedCard.text === "skip") {
                    gameLogic.playSkipCard(user_id, game_id, card_id)
                } else if (playedCard.text === "draw2") {
                    gameLogic.playDraw2Card(user_id, game_id, card_id)
                } else if (playedCard.text === "wildcard") {
                    //active player set the color
                    gameLogic.playWildcard(user_id, game_id, card_id, color)
                } else if (playedCard.text === "wildcard") { //wildcard4
                    //active player set the color and next player is skipped
                    gameLogic.playWildcard4(user_id, game_id, card_id, color)
                } else {
                    res.send("Invalid card")
                }
            }).then(() => {
                dbUtil.discardCard(user_id, game_id, card_id).then(() => {
                    dbUtil.setPlayedStatus(user_id, game_id).then(() => {
                        res.send("successful play")
                    })
                })
            })
        } else {
            res.send("Not active")
        }
    })



    router.post('/endturn', async (req, res) => {
        const game_id = 8;
        const user_id = 15;
        const currentActivePlayer = await dbUtil.getActivePlayer(game_id);
        const {actioned} = await dbUtil.getActionedStatus(game_id, user_id);
        // console.log("currentActive: " + currentActivePlayer.user_id);
        if (currentActivePlayer.user_id === user_id && actioned) {
            dbUtil.unsetActioned(user_id, game_id);
            // console.log("already actioned");
            gameLogic.passToNextPlayer(user_id, game_id);
            res.send("passed");
        } else {
            res.send("not active or not actioned");
        }
    })
    router.get('/activeplayer', (req, res) => {
        const game_id = 8;
        const user_id = 17;
        dbUtil.getActivePlayer(game_id).then(result => {
            console.log(result)
            res.send(result);
        })
    })

    router.get('/nextplayer', (req, res) => {
        const game_id = 8;
        const user_id = 17;
        gameLogic.getNextPlayer(user_id, game_id).then(result => {
            console.log(result)
            res.send('{"user_id" : ' + result + '}');
        })
    })

    router.get('/getallplayerBySeat',(req,res)=>{
        const game_id = 8;
        dbUtil.getGamePlayer(game_id).then(result => {
            console.log(result)
            res.send(result);
        })
    })

    router.get('/validate',(req,res)=>{
        const game_id = 8;
        const card_id = 11;
        gameLogic.isValid(game_id, card_id).then(result => {
            console.log(result)
            res.send(result);
        })
    })



    router.get('/gameseat',(req,res)=>{
        const game_id = 8;
        const user_id = 11;
        dbUtil.getSeat(game_id, user_id).then(data =>{
            console.log(data)
            // console.log(data.number === null);
            res.send(data)
        })
    })

    router.post('/playercard',(req,res)=>{
        const game_id = 8;
        const user_id = 11;
        dbUtil.getAllPlayerCard(game_id, user_id).then(data =>{
            res.send(data); //{"direction":false}
        })
    })

    router.post('/initialGame',(req,res)=>{
        const game_id = 8;
        dbUtil.getStartedGamePlayers(game_id).then((players)=>{
            for (let i = 0; i < 4; i++) {
                console.log(players[i].user_id);
                gameLogic.drawCard(players[i].user_id, game_id, 7);
            }
        }).catch((err)=>{
            console.log(err)
        })
        res.send("OK")
    })

    router.post('/drawcard', async (req, res) => {
        const user_id = 17;
        const game_id = 8;
        const currentActivePlayer = await dbUtil.getActivePlayer(game_id);
        if (currentActivePlayer.user_id === user_id) {
            gameLogic.drawCard(user_id, game_id, 1, function(){});
            dbUtil.setActioned(user_id, game_id).then(() => {
                res.send("OK");
            }).catch(error => {
                res.status(500).end()
            })
        } else {
            res.send("not active");
        }
    })


    router.post('/discardcard',(req,res)=>{
        const game_id = 8;
        const user_id = 11;
        const card_id = 2;
        gameLogic.playCard(user_id, game_id, card_id).then(data =>{
            res.send("OK");
        })
    })
    router.post('/pass',(req,res)=>{
        const game_id = 8;
        const user_id = 11;
        const card_id = 2;
        gameLogic.passToNextPlayer(user_id, game_id, true).then(data =>{
            res.send("OK");
        })
    })


    router.post('/testinitialization',(req,res)=>{
         const game_id = 8;

        dbUtil.getStartedGamePlayers(game_id).then((players)=>{
            if(players.length === 4){
                // gameLogic.initializeDeck(game_id).then(()=> {
                //     for (let i = 0; i < 4; i++) {
                //         gameLogic.drawCard(players[i].user_id, game_id, 7)
                //     }

                async.eachSeries(players,(player,callback)=>{
                        gameLogic.drawCard(player.user_id, game_id, 7,callback)
                },
// optional callback
                    function(err, results) {
                    if(err){
                        res.status(500).end()
                    }else {
                        res.send('ok')
                    }
                    });


                // })
            }
        }).catch((err)=>{
            console.log(err)
        })


    })




}


module.exports = route;