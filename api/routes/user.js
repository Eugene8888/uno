const crypto = require('crypto');
const db = require('../../db/index');
const  hash= (password)=>{
    return crypto.createHmac('sha256', 'csc667')
        .update(password)
        .digest('hex');
};
const route = function(router) {
    router.post('/register',(req,res)=>{
        console.log(req.body)
        let {username,password,email} = req.body;
        //check if post data is valid or not
        if(username && email && password){
            // insert new user to data
            password = hash(password)
            db.one(`INSERT INTO "public"."user" ("user_id", "username", "password", "email") VALUES (DEFAULT, '${username}', '${password}', '${email}') RETURNING user_id`)
                .then(data => {
                    console.log(data); // print new user id;
                    res.send('ok')
                })
                .catch(error => {
                    console.log('ERROR:', error); // print error;
                    res.status(500).end()
                });

        }else {
            res.status(400).send({message:"Bad request, An object with property, username , password and email must be passed in order to create new user"})
        }
    });


    router.get('/profile',(req,res)=>{
            res.send(req.user);
    })

};
module.exports = route;
