
const route = function(router) {
    router.post('/message/:gameID',(req,res)=>{
        console.log(req.body.message);
        req.io.sockets.in(req.params.gameID).emit('message', {msg: req.body.message,
            username: req.user.username

        });
        res.send(req.body.message);
    })
};
module.exports = route;