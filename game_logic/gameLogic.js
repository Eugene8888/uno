const dbUtil = require('./gameLogicDBUtils');
const async = require('async')

//initialize game
const initializeDeck = function(game_id) {
    return dbUtil.selectAllCard().then(data => {
        async.each(data, function(card, callback) {
            dbUtil.addCardToGame(game_id, card.card_id).then(()=>{
                callback()
            })
        }, function(err) {

        });

    })
};

const drawFirstCard = function(card_id, game_id) {
    dbUtil.getCard(card_id).then(card => {
        console.log("card: " + card)
        dbUtil.setGameColor(card.color, game_id);
        dbUtil.setGameNumber(card.number, game_id);
        dbUtil.setGameTopCard(card_id, game_id);
    })
}

//game user actions
// const drawCard = function(user_id, game_id, numOfCard,callback) {
const drawCard = function(user_id, game_id, numOfCard,callback) {
    //check if there is enough cards to draw
    dbUtil.getAllAvailiableCardCount(game_id).then(count => {
        // console.log("availiable card: " + count.count)
        if (count.count < numOfCard) { // not enough card, reset
            console.log("not enough card; reshuffle")
            dbUtil.restoreDiscard(game_id).then(response => {
                dbUtil.randomCards(game_id, numOfCard).then(data => {
                    for (let i = 0; i < data.length; i++) {

                        dbUtil.addCardToGameUser(user_id, game_id, data[i].card_id);
                    }
                })
            }).then(()=>{
                callback(null)
            }).catch((err)=>{
                callback(err)
            })
        } else { //enough card, draw
            dbUtil.randomCards(game_id, numOfCard).then(data => {

                for (let i = 0; i < data.length; i++) {
                    dbUtil.addCardToGameUser(user_id, game_id, data[i].card_id);
                }
            }).then(()=>{
                callback(null)
            }).catch((err)=>{
                callback(err)
            })
        }
    })
}

const getNextPlayer = async function(userid, gameid) {
// const getNextPlayer = async function(userid, gameid) {
    const{direction} = await dbUtil.getGameDir(gameid);
    const{seat} = await dbUtil.getSeat(gameid, userid);
    const nextseat = await direction? (seat + 2) % 4 + 1 : seat % 4 + 1;
    const {user_id} = await dbUtil.getPlayerBySeat(gameid, nextseat);
    return user_id;
}

const passToNextPlayer = async function(userId, gameId,cb) {
    dbUtil.unsetActivePlayer(userId, gameId).then(async() => {
        const nextPlayer = await getNextPlayer(userId, gameId);
        const nextTwoPlayer = await getNextPlayer(nextPlayer, gameId);
        const {skip} = await dbUtil.getSkipStatus(gameId, nextPlayer);
        // console.log("skipped status:" + skip)
        if (skip) {
          dbUtil.unsetSkip(nextPlayer, gameId).then(() => {

                // const nextActivePlayer = await getNextPlayer(userId, gameId);
                dbUtil.setActivePlayer(nextTwoPlayer, gameId).then(()=>{
                    cb(nextTwoPlayer)
                });
                // console.log("Skip happens!!! " + nextTwoPlayer);
            })
        } else {
            dbUtil.setActivePlayer(nextPlayer, gameId).then(()=>{
                cb(nextPlayer)
            });
        }

    })
}

const isValid = async function(game_id, user_id, card_id) {
    const {color} = await dbUtil.getGameColor(game_id);
    const {number} = await dbUtil.getGameNumber(game_id);
    console.log(color + ", " + number);
    const {played} = await dbUtil.getPlayedStatus(game_id, user_id);
    console.log(color + ", " + number + ", " + played);
    console.log(card_id)
    const card = await dbUtil.getCard(card_id);
    console.log(card)
    return (!played && (card.text === "wildcard" || card.text === "wildcard4" || card.color === color || card.number === number));
}


const playNumberCard = async function(user_id, game_id, card_id) {
    //TODO: fix set color
    const card = await dbUtil.getCard(card_id);
    // const {color} = await dbUtil.getGameColor(game_id);
    // const {number} = await dbUtil.getGameNumber(game_id);
    dbUtil.setGameColor(card.color, game_id).then(response => {
        dbUtil.setGameNumber(card.number, game_id).then(response => {
            dbUtil.discardCard(user_id, game_id, card_id);
        })
    })
}

const playReverseCard = async function(user_id, game_id, card_id) {
    const {direction} = await dbUtil.getGameDir(game_id);
    dbUtil.setGameDir(!direction, game_id).then(() => {
        dbUtil.discardCard(user_id, game_id, card_id)
    })
}

const playSkipCard = async function (user_id, game_id, card_id) {
    const nextPlayer = await getNextPlayer(user_id, game_id);
    dbUtil.setSkip(nextPlayer, game_id).then(() => {
        dbUtil.discardCard(user_id, game_id, card_id);
    })
}

const playDraw2Card = async function (user_id, game_id, card_id, nextPlayer) {
    // const nextPlayer = await getNextPlayer(user_id, game_id);
    drawCard(nextPlayer, game_id, 2, function(){});
}

const playWildcard = function(user_id, game_id, card_id, newcolor) {
    dbUtil.setGameColor(newcolor, game_id)
}

const playWildcard4 = async function (user_id, game_id, card_id, newcolor, nextPlayer) {
    // const nextPlayer = await getNextPlayer(user_id, game_id);
    drawCard(nextPlayer, game_id, 4, function(){}).then(() => {
        dbUtil.setGameColor(newcolor, game_id);
    })
}

const finishGame = async function (user_id, game_id) {
    dbUtil.deleteGameCards(game_id).then(() => {
        dbUtil.deleteGameUsers(game_id).then(() => {
            dbUtil.deleteGame(game_id);
        })
    })
}
module.exports = {
    initializeDeck,
    drawFirstCard,
    drawCard,
    getNextPlayer,
    passToNextPlayer,
    isValid,
    playNumberCard,
    playReverseCard,
    playSkipCard,
    playDraw2Card,
    playWildcard,
    playWildcard4,
    finishGame
};
// }