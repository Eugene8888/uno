const db = require('../db/index');

//initialize game: select all cards and add all of them to the game
const selectAllCard_query = 'SELECT card_id FROM card';
const selectAllCard = () =>
    db.any(selectAllCard_query);
const addCardToGame_query = 'INSERT INTO game_user_card (game_id,card_id) VALUES (${gameid},${cardid})';
const addCardToGame = (gameid,cardid) =>
    db.none(addCardToGame_query, {gameid,cardid});

const addUserToGame_query = 'INSERT INTO game_user (user_id,username,game_id,seat) VALUES (${userid},${username},${gameid},${seat})';
const addUserToGame = (userid,username,gameid,seat)=>{
    return db.none(addUserToGame_query, {userid,username,gameid,seat});
}



//finish the game
const deleteGameUsers_query = 'DELETE FROM game_user WHERE game_id = (${gameid})';
const deleteGameUsers = (gameid) =>
    db.none(deleteGameUsers_query, {gameid});
const deleteGame_query = 'DELETE FROM game WHERE game_id = (${gameid})';
const deleteGame = (gameid) =>
    db.none(deleteGame_query, {gameid});
const deleteGameCards_query = 'DELETE FROM game_user_card WHERE game_id = (${gameid})';
const deleteGameCards = (gameid) =>
    db.none(deleteGameCards_query, {gameid});

//get game attributes
    //*game table
const getGameDir_query = 'SELECT direction FROM game WHERE game_id = (${gameid})'; //false: clockwise   true: conterclockwise
const getGameDir = (gameid) =>
    db.one(getGameDir_query, {gameid});

const getGameColor_query = 'SELECT color FROM game WHERE game_id = (${gameid})';
const getGameColor =  (gameid) =>
    db.one(getGameColor_query, {gameid}); //{ color: 'R' }

const getGameNumber_query = 'SELECT number FROM game WHERE game_id = (${gameid})';
const getGameNumber = (gameid) =>
    db.one(getGameNumber_query, {gameid}); //{ color: '' }

const getGameState_query = 'SELECT state FROM game WHERE game_id = (${gameid})';
const getGameState = (gameid) =>
    db.one(getGameState_query, {gameid});

const getGame_query = 'SELECT * FROM game WHERE game_id = (${gameid})';
const getGame = (gameid) =>
    db.one(getGame_query, {gameid});


    //*game_user table
const getGamePlayers_query = 'SELECT * FROM game_user WHERE game_id = (${gameid}) ORDER BY seat';
const getGamePlayer = (gameid) =>
    db.any(getGamePlayers_query, {gameid});

const getStartedGamePlayers_query = 'SELECT user_id FROM game_user WHERE game_id = (${gameid}) AND start_game = true';
const getStartedGamePlayers = (gameid) =>
    db.any(getStartedGamePlayers_query, {gameid});

const getActivePlayer_query = 'SELECT user_id FROM game_user WHERE game_id = (${gameid}) AND active = true';
const getActivePlayer = (gameid) =>
    db.one(getActivePlayer_query, {gameid});

const getIsInGame_query = 'SELECT COUNT(*) FROM game_user WHERE game_id = (${gameid}) AND user_id = (${userid})';
const getIsInGame = (gameid, userid) =>
    db.one(getIsInGame_query, {gameid, userid});

const getSeat_query = 'SELECT seat FROM game_user WHERE game_id = (${gameid}) AND user_id = (${userid})';
const getSeat = (gameid, userid) =>
    db.one(getSeat_query, {gameid, userid});

const getPlayerBySeat_query = 'SELECT user_id FROM game_user WHERE game_id = (${gameid}) AND seat = (${seat})';
const getPlayerBySeat = (gameid, seat) =>
    db.one(getPlayerBySeat_query, {gameid, seat});

const getSkipStatus_query = 'SELECT skip FROM game_user WHERE game_id = (${gameid}) AND user_id = (${userid})';
const getSkipStatus = (gameid, userid) =>
    db.one(getSkipStatus_query, {gameid, userid});

const getActionedStatus_query = 'SELECT actioned FROM game_user WHERE game_id = (${gameid}) AND user_id = (${userid})';
const getActionedStatus = (gameid, userid) =>
    db.one(getActionedStatus_query, {gameid, userid});

const getPlayedStatus_query = 'SELECT played FROM game_user WHERE game_id = (${gameid}) AND user_id = (${userid})';
const getPlayedStatus = (gameid, userid) =>
    db.one(getPlayedStatus_query, {gameid, userid});



//*card table
const getCard_query = 'SELECT * FROM card WHERE card_id = (${cardid})';
const getCard = (cardid) =>
    db.one(getCard_query, {cardid});

    //*game_user_card table

//get all cards in the draw pile
const getAllAvailiableCardCount_query = 'SELECT COUNT(*) FROM game_user_card WHERE game_id = (${gameid}) AND user_id IS null';
const getAllAvailiableCardCount = (gameid) =>
    db.one(getAllAvailiableCardCount_query, {gameid});

//get all cards of a game_user
const getAllPlayerCard_query = 'SELECT card_id FROM game_user_card WHERE game_id = (${gameid}) AND user_id = (${userid}) AND discard = false';
const getAllPlayerCard = (gameid, userid) =>
    db.any(getAllPlayerCard_query, {gameid,userid});


//set game attributes
const setGameDir_query = 'UPDATE game SET direction = (${direction}) WHERE game_id = (${gameid})';
const setGameDir = (direction, gameid) =>
    db.none(setGameDir_query, {direction, gameid});

const setGameColor_query = 'UPDATE game SET color = (${color}) WHERE game_id = (${gameid})';
const setGameColor = (color, gameid) =>
    db.none(setGameColor_query, {color, gameid});

const setGameNumber_query = 'UPDATE game SET number = (${number}) WHERE game_id = (${gameid})';
const setGameNumber = (number, gameid) =>
    db.none(setGameNumber_query, {number, gameid});

const setGameTopCard_query = 'UPDATE game SET top_card = (${card_id}) WHERE game_id = (${gameid})';
const setGameTopCard = (card_id, gameid) =>
    db.none(setGameTopCard_query, {card_id, gameid});

const setGameStart_query = 'UPDATE game SET state = true WHERE game_id = (${gameid})';
const setGameStart = (gameid) =>
    db.none(setGameStart_query, {gameid});

const setPlayerStart_query = 'UPDATE game_user SET start_game = true WHERE user_id = (${userid}) AND game_id = (${gameid})';
const setPlayerStart = (userid, gameid) =>
    db.none(setPlayerStart_query, {userid, gameid});

const setActivePlayer_query = 'UPDATE game_user SET active = true WHERE user_id = (${userid}) AND game_id = (${gameid}) RETURNING user_id';
const setActivePlayer = (userid, gameid) =>
    db.one(setActivePlayer_query, {userid, gameid});

const unsetActivePlayer_query = 'UPDATE game_user SET active = false WHERE user_id = (${userid}) AND game_id = (${gameid})';
const unsetActivePlayer = (userid, gameid) =>
    db.none(unsetActivePlayer_query, {userid, gameid});

const setSkip_query = 'UPDATE game_user SET skip = true WHERE user_id = (${userid}) AND game_id = (${gameid})';
const setSkip = (userid, gameid) =>
    db.none(setSkip_query, {userid, gameid});

const unsetSkip_query = 'UPDATE game_user SET skip = false WHERE user_id = (${userid}) AND game_id = (${gameid})';
const unsetSkip = (userid, gameid) =>
    db.none(unsetSkip_query, {userid, gameid});

const setActioned_query = 'UPDATE game_user SET actioned = true WHERE user_id = (${userid}) AND game_id = (${gameid})';
const setActioned = (userid, gameid) =>
    db.none(setActioned_query, {userid, gameid});

const unsetActioned_query = 'UPDATE game_user SET played = false, actioned = false WHERE user_id = (${userid}) AND game_id = (${gameid})';
const unsetActioned = (userid, gameid) =>
    db.none(unsetActioned_query, {userid, gameid});

const setPlayedStatus_query = 'UPDATE game_user SET played = true, actioned = true WHERE user_id = (${userid}) AND game_id = (${gameid})';

const setPlayedStatus = (userid, gameid) =>
    db.none(setPlayedStatus_query, {userid, gameid});

const unsetPlayedStatus_query = 'UPDATE game_user SET played = false, actioned = false WHERE user_id = (${userid}) AND game_id = (${gameid})';
const unsetPlayedStatus = (userid, gameid) =>
    db.none(unsetPlayedStatus_query, {userid, gameid});




//user draws a card
const randomCards_query = 'SELECT card_id FROM game_user_card WHERE game_id = (${gameid}) AND user_id IS null ORDER BY RANDOM() LIMIT (${numOfCard})';
const randomCards = (gameid, numOfCard) =>
    db.any(randomCards_query, {gameid, numOfCard});

const addCardToGameUser_query = 'UPDATE game_user_card SET user_id = (${userid}) WHERE game_id = (${gameid}) AND card_id = (${cardid})';
const addCardToGameUser = (userid,gameid,cardid,cb) => db.none(addCardToGameUser_query, {userid, gameid, cardid});



//user plays a card
const discardCard_query = 'UPDATE game_user_card SET discard = true WHERE user_id = (${userid}) AND game_id = (${gameid}) AND card_id = (${cardid})';
const discardCard = (userid,gameid,cardid) =>
    db.none(discardCard_query, {userid, gameid, cardid});

//When there is not enough card to draw, restore all discarded cards to the draw pile
const restoreDiscard_query = 'UPDATE game_user_card SET user_id = NULL, discard = DEFAULT WHERE game_id = (${gameid}) AND user_id IS NOT NULL AND discard = true';
// const restoreDiscard_query = 'UPDATE game_user_card SET discard = false WHERE game_id = (${gameid}) AND user_id IS NOT NULL AND discard = true';
const restoreDiscard = (gameid) =>
    db.none(restoreDiscard_query, {gameid});





module.exports = {
    selectAllCard,
    addCardToGame,
    addUserToGame,

    deleteGameUsers,
    deleteGame,
    deleteGameCards,
    getGame,
    getGameDir,
    getGameColor,
    getGameNumber,
    getGameState,
//game_user
    getGamePlayer,
    getStartedGamePlayers,
    getActivePlayer,
    getIsInGame,
    getSeat,
    getPlayerBySeat,
    getSkipStatus,
    getActionedStatus,
    getPlayedStatus,
//card
    getCard,
//game_user_card
    getAllAvailiableCardCount,
    getAllPlayerCard,

    setGameDir,
    setGameColor,
    setGameNumber,
    setGameTopCard,
    setGameStart,
    setPlayerStart,
    setActivePlayer,
    unsetActivePlayer,
    setSkip,
    unsetSkip,
    setActioned,
    unsetActioned,
    setPlayedStatus,
    unsetPlayedStatus,

    randomCards,
    addCardToGameUser,
    discardCard,
    restoreDiscard

};