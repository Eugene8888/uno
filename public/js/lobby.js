let username ;
$( document ).ready(()=>{

    axios.get('/api/v1/profile').then((response)=>{
        username = response.data.username;
    }).catch((err)=>{
        console.log(err)
    })
    axios.get('/api/v1/gameroom').then((response)=>{
        console.log(response.data)
        response.data.forEach((game)=>{
            $('#lobby-games').append(
                ' <li class = "list-group-item">Game '+game.game_id+'<button onclick="joinGame('+game.game_id+')" class = "btn btn-success join-button">Join game</button> </li>'
            )
        })
    }).catch((err)=>{
        console.log(err)
    })

});

function joinGame(game_id){
    console.log(game_id)
    axios.post('/api/v1/joingame',{game_id}).then((response)=>{
        window.location.replace('/game/'+game_id)
    }).catch((err)=>{
        console.log(err)
    })
}


function onKeydown(event){
    if(event.key==='Enter'){
        $user_form.submit(function (err) {
            err.preventDefault();
            console.log("User Form Submitted");
            socket.emit('new user', $username.val(), function(data){
                if(data){
                    $user_form.hide();
                }
            });
            $username.val('');
        });
    }

}
var socket = io.connect();
var $message_form = $('#message_form');
var $message = $('#message');
var $chat = $('#chat');
var $user_container = $('#user-container');
var $user_form = $('#user_form');
var $users = $('#users');
var $username = $('#username');

$message_form.submit(function (err) {
    err.preventDefault();
    console.log("Form Submitted");
    socket.emit('chat:lobby', {message:$message.val(),username});
    $message.val('');
});
$user_form.submit(function (err) {
    err.preventDefault();
    console.log("User Form Submitted");
    socket.emit('new user', $username.val(), function(data){
        if(data){
            $user_form.hide();
        }
    });
    $username.val('');
});
function createGame(){
    console.log('...creating...')
    axios.post('/api/v1/newgame').then((response)=>{
        console.log(response.data)
        $('#lobby-games').append(
            ' <li class = "list-group-item">Game '+response.data.game_id+'<button onclick="joinGame('+response.data.game_id+')" class = "btn btn-success join-button">Join game</button> </li>'
        )

    }).catch((err)=>{
        console.log(err)
    })
}
// $game_form.submit(function (err) {
//     err.preventDefault();
//     console.log("Game Created");
//     socket.emit('new game', $game.val(), function(data){
//         if(data){
//             $user_form.hide();
//         }
//     });
//     $username.val('');
// });
function checkEnter(e){
    console.log(e.key)
}
socket.on('chat:lobby:response', function(data){

    $chat.append('<div class = "well text-left"><strong>'+ data.username+ '</strong>: '+ data.msg +'</div>');

});