var socket = io();
let $draw_card = $('#draw_card').hide();
let $player_hand = $('#player_hand');
let $start_game = $('#start_game');
let $start_game_button = $('#start_game_button');
let $top_card_container = $('#top_card_container');
let $end_turn_button = $('#end_turn_button').hide()
let game_id;
let game_start = false;
let global_player_seat;
let user;
let left ,right,front;

function setActivePlayer(user_id){
    if(user_id === left){
        $('#table_left_player').css("background-color", "yellow");
        $('#table_right_player').css("background-color", "white");
        $('#table_front_player').css("background-color", "white");
    } else if(user_id === right){
        $('#table_right_player').css("background-color", "yellow");
        $('#table_left_player').css("background-color", "white");
        $('#table_front_player').css("background-color", "white");
    }
    else  if(user_id === front){
        $('#table_front_player').css("background-color", "yellow");
        $('#table_left_player').css("background-color", "white");
        $('#table_right_player').css("background-color", "white");
    }else{
        $('#table_front_player').css("background-color", "white");
        $('#table_left_player').css("background-color", "white");
        $('#table_right_player').css("background-color", "white");
    }
}

function initGame(){
    console.log('api init game')
    axios.get('/api/v1/gameroom/'+game_id+'/getAllGameInfo').then( (response)=>{
        console.log('allinfo',response.data)

        const {allGamePlayers,game,currentActive} = response.data
        updatePlayers(allGamePlayers);
        console.log('game info ',game);
        if(game.top_card){
            console.log(game.top_card)
            $("#topCard").attr("src",'/img/card/'+game.top_card+'.png');
        }
        setActivePlayer(currentActive.user_id);
        update_opponent_cards(
            allGamePlayers.find((user)=>{
                return user.user_id === left
            }).cards.length,
            allGamePlayers.find((user)=>{
                return user.user_id === front
            }).cards.length,
            allGamePlayers.find((user)=>{
                return user.user_id === right
            }).cards.length
        )
        console.log(allGamePlayers.find((current)=>{
            return current.user_id = user.user_id
        }))
        if(game.state){
            $start_game_button.hide();
            $draw_card.show()
            $end_turn_button.show()
        }
    }).catch((err)=>{
        console.log(err)
    })
}
$('document').ready(()=>{
    //get userinfo
    axios.get('/api/v1/profile').then(({data})=>{
        user =data
    }).catch((err)=>{
        console.log(err)
    });



    let url = window.location.href;
    console.log(url);
    let index = url.lastIndexOf('/');
    game_id = url.substr(index+1,url.length);
    initGame();

    axios.get('/api/v1/gameroom/'+game_id+'/getallplayers').then((response)=>{
        console.log('users',response.data)
        updatePlayers(response.data)
    }).catch((err)=>{
        console.log(err);
    })

    console.log('game id',game_id);
    socket.on('reconnect', () => {
        socket.emit('room', game_id)
    })
    socket.on('connect',async function() {
        // Connected, let's sign-up for to receive messages for this room
        await socket.emit('room', game_id);
    });
    socket.on('message', function(data) {
        $('#chat').append('<div class = "well text-left"><strong>'+ data.username+ '</strong>: '+ data.msg +'</div>');
    });
    socket.on('game:card', function(data) {
        if(data.type === 'update'){
            console.log('playing card',data)
            let {game,topcard,allGamePlayers} =data;
            $("#topCard").attr("src",'/img/card/'+topcard+'.png');
            // console.log('idsssssssss',data.user.user_id, user.user_id)

            allGamePlayers.forEach((player)=>{
                if(player.user_id === user.user_id){
                    console.log('updating ............... ok')
                    update_cards(player.cards)
                }
            })

            update_opponent_cards(allGamePlayers.find((player)=>{
                return player.user_id === left
            }).cards.length,allGamePlayers.find((player)=>{
                return player.user_id === front
            }).cards.length,allGamePlayers.find((player)=>{
                return player.user_id === right
            }).cards.length)
            // if(data.user.user_id === user.user_id){
            //     update_cards(data.user.cards)
            // }else {
            //     if(data.user.user_id === left){
            //         $("#opponent_left_hand li:first-of-type").remove();
            //     }
            //     if(data.user.user_id === right){
            //         $("#opponent_right_hand li:first-of-type").remove();
            //     }
            //     if(data.user.user_id === front){
            //         $("#opponent_front_hand li:first-of-type").remove();
            //     }
            //
            // }
        }
        if(data.type === 'drawcard'){
            console.log(data);
            switch (data.data.user_id) {
                case left:
                    $("#opponent_left_hand").append('<li class = "card_container_horizontal"> <input type ="image" src="/img/card/unoBackHorizontal.png" /> </li>')
                case right:
                    $("#opponent_right_hand").append('<li class = "card_container_horizontal"> <input type ="image" src="/img/card/unoBackHorizontal.png" /> </li>')
                case front :
                    $("#opponent_front_hand").append('<li class = "card_container_horizontal"> <input type ="image" src="/img/card/unoBack.png" /> </li>')
            }
            // check user id and update
            // update drawcard
            // update_cards();
            if(data.data.user_id ===user.id){
                console.log('updating ... my card')
                console.log(data.data);
                update_cards(data.data.updatedCardSet)
            }

        }
        // $('#chat').append('<div class = "well text-left"><strong>'+ data.username+ '</strong>: '+ data.msg +'</div>');
    });

    socket.on("game:allplayers",({players})=>{
        updatePlayers(players)
    });

    socket.on("game:turn",(data)=>{
        setActivePlayer(data.activePlayer_id)
    })
    socket.on('game:status',function(data) {
        if(data.status === 'Game Over'){
            alert('Game Over')
        }
    })

    socket.on("game:start",(data)=>{
        initGame()
        // console.log(data);
        // let p_img = "/img/card/"+data.topCard + ".png";
        // let el = '<div class = "card_container"> <input type ="image" id = "'+data.topCard+'" src="'+p_img+'" onClick="sendCard(this.id)"/></div>';
        // document.getElementById('top_card_container').innerHTML = el;
        // $draw_card.show();
        // $end_turn_button.show();
        // $start_game_button.hide();
        //
        // let currentUser = data.players.find((current)=>{
        //     current.user_id = user.user_id
        // });
        // console.log('current',currentUser)
        //  update_cards(currentUser.cards)
        // update_opponent_cards();
    })
    //Check to see if game has started then Update cards,game information and opponent card length.

    //Calls api to get player seat number
    // axios.get('/api/v1/gameroom/'+game_id+'/getseat').then((response)=>{
    //     console.log("You are in seat number:" + response.data.seat);
    //     let player_seat = response.data.seat;
    //     global_player_seat = player_seat;
    //     let left_seat = (player_seat) % 4 + 1;
    //     let front_seat = (player_seat+1) % 4 + 1;
    //     let right_seat = (player_seat+2) % 4 + 1;
    //     $('#table_left_player').html('<h2>Player '+left_seat+'</h2>');
    //     $('#table_front_player').html('<h2>Player '+front_seat+'</h2>');
    //     $('#table_right_player').html('<h2>Player '+right_seat+'</h2>');
    // });
});
//Function to run when end turn button is pressed.


function  updatePlayers(players){
    let myinfo = players.find((p)=>{
        return p.user_id === user.user_id
    });
    console.log('myinfo',myinfo)
    if(myinfo.cards){
        update_cards(myinfo.cards)
    }

    let left_seat = (myinfo.seat) % 4 + 1;
    let front_seat = (myinfo.seat+1) % 4 + 1;
    let right_seat = (myinfo.seat+2) % 4 + 1;
    console.log(left_seat)
    console.log(front_seat)
    console.log(right_seat)
    left = players[left_seat-1]&&players[left_seat-1].user_id ? players[left_seat-1].user_id:"";
    right = players[right_seat-1] && players[right_seat-1].user_id?players[right_seat-1].user_id:"";
    front = players[front_seat-1] &&players[front_seat-1].user_id?players[front_seat-1].user_id:"";
    if(left_seat-1 < players.length){
        $('#table_left_player').html('<h2>'+players[left_seat-1] && players[left_seat-1].username?players[left_seat-1].username:""+'</h2>');
    }

    if(front_seat-1 < players.length){
        $('#table_front_player').html('<h2>'+players[front_seat-1] && players[front_seat-1].username?players[front_seat-1].username:""+'</h2>');
    }
    if(right_seat-1 < players.length){
        $('#table_right_player').html('<h2>'+players[right_seat-1] && players[front_seat-1].username?players[right_seat-1].username:""+'</h2>');
    }


}
function end_turn(){

    axios.post('/api/v1/gameroom/'+game_id+'/endturn').then((response)=>{
        console.log("Turn ended");
        // socket.emit('end_turn')
    }).catch((err)=>{
        console.log(err);
    })
}

//Function triggered when a card is selected.
function sendCard(id) {
    console.log(id)
    let data = '';
    let p_value =id;
    let p_img;
    let buttons = '';
    // for (i = 0; i < player_cards.length;i++){
    //     if(player_cards[i].card_id == id){
    //         alert(player_cards[i].card_id);
    //         console.log(player_cards[i]);
    //         p_value = player_cards[i].card_id;
    //         // p_img = "/img/card/"+p_value + ".png";
    //         // data = '<div class = "card_container"> <input type ="image" id = "'+p_value+'" src="'+p_img+'" onClick="sendCard(this.id)"/></div>';
    //         //if wildcard trigger trigger modal for setColor
    if(p_value >= 101 && p_value <= 108){
        buttons +='<button type="button" class="btn btn-danger" onclick="setColor('+p_value+',\'Y\')">Yellow</button>';
        buttons +='<button type="button" class="btn btn-danger" onclick="setColor('+p_value+',\'R\')">Red</button>';
        buttons +='<button type="button" class="btn btn-danger" onclick="setColor('+p_value+',\'G\')">Green</button>';
        buttons +='<button type="button" class="btn btn-danger" onclick="setColor('+p_value+',\'B\')">Blue</button>';
        $('#color_button').html(buttons);
        $('#choose_color_prompt').modal({show: true});
    }
    else{
        //for all other cards
        axios.post('/api/v1/gameroom/'+game_id+'/playcard',{card_id: p_value}).then((response)=>{
            console.log(response.data.valid);
            if(response.data.valid === true){
                // socket.emit('play_card',{card_id: p_value});

                // update_cards();
                // update_game_information();
                // if(response.data.gameover === true){
                //     gameOver();
                // }
            }
        })
        // }
    }
    // }
}
//form to send messages
$('#message_form').submit((e)=>{
    e.preventDefault();
    axios.post('/api/v1/message/'+game_id,{message:$('#message').val()}).then((response)=>{
        $('#message').val('')
    }).catch((err)=>{
        console.log(err)
    })
});
//function triggered when clicking start game button
$start_game.submit(function(err){
    err.preventDefault();
    console.log("Game Starting...");
    axios.put('/api/v1/gameroom/'+game_id+'/ready').then((response)=>{
        $start_game_button.hide();
        $draw_card.show();
    }).catch((err)=>{
        console.log(err)
    });
    socket.emit('start')
});
//function triggered when drawing a card
$draw_card.submit(async function (err) {
    err.preventDefault();
    console.log("Drawing...");
    await axios.post('/api/v1/gameroom/'+ game_id +'/drawcard').then((response)=>{
    });
    update_cards();
});
//Updates Current Number,Color,Direction and Active Player displayed

//updates length of opponent cards
function update_opponent_cards(opponent_left_length=7,opponent_front_length=7,opponent_right_length=7) {
    //todo link each card to the corresponding opponent
    let left_hand = '';
    let right_hand = '';
    let front_hand = '';
    for (i = 0; i < opponent_left_length; i++) {
        left_hand += '<li class = "card_container_horizontal"> <input type ="image" src="/img/card/unoBackHorizontal.png" /> </li>'
    }
    for (i = 0; i < opponent_front_length; i++) {
        front_hand += '<li class = "card_container"> <input type ="image" src="/img/card/unoBack.png" /> </li>'
    }
    for (i = 0; i < opponent_right_length; i++) {
        right_hand += '<li class = "card_container_horizontal"> <input type ="image" src="/img/card/unoBackHorizontal.png" /> </li>'
    }
    $('#opponent_left_hand').html(left_hand);
    $('#opponent_right_hand').html(right_hand);
    $('#opponent_front_hand').html(front_hand);
}
//Updates current cards player has on hand using database.
function update_cards(player_cards){
    console.log('update',player_cards)
    let html ='';
    for(i = 0; i < player_cards.length; i++){
        let p_value = player_cards[i].card_id;
        let p_img = "/img/card/"+p_value + ".png";
        html += '<li class = "card_container"> <input type ="image" id = "'+p_value+'" src="'+p_img+'" onClick="sendCard(this.id)"/></li>'
    }
    $player_hand.html(html);
}
//Function to trigger game over.
function gameOver(){
    // socket.emit('game over')
    $('#game_over_pop').modal({show:true})
}