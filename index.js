const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const app = express();
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const api = require('./api');
require('dotenv').config();
const db = require('./db/index');
const path = require('path');
const crypto = require('crypto');
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const port = process.env.PORT  || 8082;
const ip   = process.env.IP    || '0.0.0.0';
let connections=[];
//passport config
const  hash= (password)=>{
    return crypto.createHmac('sha256', 'csc667')
        .update(password)
        .digest('hex');
};
const  ensureAuthenticated=(req, res, next)=> {
    console.log(req.isAuthenticated())
    if (req.isAuthenticated())
        return next();
    else
        res.redirect('/login')

        }



passport.use(new LocalStrategy(
    { usernameField: 'email',
        passwordField: 'password',},
    function(username, password, done) {
        password = hash(password)
        db.any(`SELECT * FROM "user" WHERE email = '${username}' AND password = '${password}'`)
            .then(data => {
                console.log(data);
                done(null,data[0])
            })
            .catch(error => {
                console.log('ERROR:', error);
                done(error,null)
            });
    }
));



app.use(express.static(__dirname+'/public'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(session({
    secret:'csc667',
    resave: true,
    saveUninitialized: true,
    cookie:{

        maxAge: 300000
    }
}));
app.use(passport.initialize());
app.use(passport.session());
passport.serializeUser(function(user, done) {
    done(null, user.user_id);
});

passport.deserializeUser(function(id, done) {
    console.log(id)
    db.one(`SELECT * FROM "user" WHERE user_id = '${id}'`).then((user)=>{
        done(null,user)
    }).catch((err)=>{
        console.log(err);
        done(err,null)
    })
    ;
});

// socket io

io.on('connection', function(socket) {
     console.log('Connected %s sockets connected', connections.length);

    socket.on('room',  function(room) {
       socket.join(room,function(){
           console.log('joined '+ room)
       });

    });
    socket.on('error', function (err) {
        console.log("Socket.IO Error");
        console.log(err.stack); // this is changed from your code in last comment
    });
    socket.on('chat:lobby', function(data){
        console.log(data);
        io.emit('chat:lobby:response', {msg: data.message, username: data.username});
    });

})
app.use(function(req,res,next){
    req.io = io;
    next();
})
app.use(function(req,res,next){
    // req.user = {user_id:11,username:'Eugene Li'}
    next();
})

app.use('/api/v1',ensureAuthenticated,api);
// app.use('/api/v1',api);

app.post('/login', passport.authenticate('local', { successRedirect: '/lobby',
    failureRedirect: '/login' }));

// app.post('/login',(req,res)=>{
//     console.log(req.body)
// })
app.get('/login',(req,res)=>{
    res.sendFile(path.join(__dirname+'/public/login.html'))
})
app.get('/register',(req,res)=>{
    res.sendFile(path.join(__dirname+'/public/register.html'))
})

app.get('/game/:id',ensureAuthenticated,(req,res)=>{
    res.sendFile(path.join(__dirname+'/public/game.html'))
})
app.get('/lobby', ensureAuthenticated,(req,res)=>{
    res.sendFile(path.join(__dirname+'/public/lobby.html'))
});
app.get('/',(req,res)=>{
    if(req.user){
        res.redirect('/lobby')
    }else {
        res.redirect('/login')
    }

     // res.sendFile(path.join(__dirname+'/public/index.html'))
});

// app.use('/tests', require('./routes/tests'));
app.use(function(req,res,next)
{

    var err = new Error('Not Found');
    err.status = 404;
    res.send('page not found')
    // res.sendFile(path.join(__dirname+'/public/404.html'))

})
//hanlde server err 500 
app.use(function(err){
    res.status(err.status || 500)
        .send({message: err.message})
});




// app.listen(port,ip);
http.listen(port, function(){
    console.log('Server running at port ' + port)
});
